#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>
#include <QStringBuilder>
#include <QLocale>
#include <QVector>
#include <QThread>
#include <QActionGroup>
#include <QFileDialog>
#include "simulation.h"
#include "script.h"
#include "scriptdialog.h"
#include "progressdialog.h"
#include "carfactory.h"

const double HIST_TIME = 20.0;   // s
const double XTICK_STEP = HIST_TIME / 5.0;
const double DISP_DIST = 10.0;   // m
const double YTICK_STEP = DISP_DIST / 5.0;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void simulate();
    void restartSim();
    void about();
    void aboutQt();
    void loadScript();
    void scriptFinished(double totalDist, double totalTime, double error, const QVector<double>& keys, const QVector<double>& vals, double minCur, double maxCur);

private:
    QString fmt(double val);
    CarType getSelectedCarType();
    void updateGUI();

    ScriptDialog diag;
    ProgressDialog prog;
    QTimer timer;
    QActionGroup ctrlGroup;
    QLocale locale;

    Ui::MainWindow *ui;
    Simulation* sim;
};

#endif // MAINWINDOW_H

#ifndef SCRIPT_H
#define SCRIPT_H

#include <QObject>
#include <QVector>
#include <QTextStream>
#include <QFile>
#include <QStringList>
#include <QDebug>
#include <QtAlgorithms>
#include <QVector>
#include "simulation.h"
#include "car.h"
#include <cmath>

const int MAX_STEPS = 300000;

struct ScriptLine {
    double pos;     // m
    double speed;   // m/s
    double load;    // N.m

    ScriptLine();
    ScriptLine(double pos_);

    bool operator<(const ScriptLine& o) const;
};

class Script : public QObject {
    Q_OBJECT

public:
    // will take ownership of the cars
    explicit Script(Car* car, Car* guide);
    virtual ~Script();

    bool load(const QString &path);
    bool query(double pos, double& speed, double& load) const;

public slots:
    void simulate();

signals:
    void progress(double value);
    void finished(double totalDist, double totalTime, double error,
                  const QVector<double>& keys, const QVector<double>& vals,
                  double minCur, double maxCur);

private:
    Simulation sim;
    QVector<ScriptLine> lines;
    double targetDistance, endPosition;
    int nLines;

};

#endif // SCRIPT_H

#-------------------------------------------------
#
# Project created by QtCreator 2013-11-11T14:42:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = FuzzyCars
TEMPLATE = app

CONFIG += c++11

win32 {
    INCLUDEPATH += C:\dev\fuzzylite
    LIBS += -LC:\dev\fuzzylite
}

LIBS += -lfuzzylite

FORMS    += mainwindow.ui \
    scriptdialog.ui \
    progressdialog.ui

SOURCES += main.cpp\
        mainwindow.cpp \
    pid.cpp \
    mathutils.cpp \
    motor.cpp \
    simulation.cpp \
    qcustomplot.cpp \
    car.cpp \
    fuzzydistancecontroller.cpp \
    fuzzyspeedcontroller.cpp \
    script.cpp \
    scriptdialog.cpp \
    progressdialog.cpp \
    fuzzycar.cpp \
    carfactory.cpp \
    pidcar.cpp

HEADERS  += mainwindow.h \
    pid.h \
    motor.h \
    simulation.h \
    qcustomplot.h \
    car.h \
    fuzzydistancecontroller.h \
    fuzzyspeedcontroller.h \
    script.h \
    scriptdialog.h \
    mathutils.h \
    progressdialog.h \
    fuzzycar.h \
    carfactory.h \
    pidcar.h

OTHER_FILES += export_speed.cpp \
    export_distance.cpp \
    fcl/speed.fcl \
    fcl/distance.fcl \
    scripts/plain.car \
    scripts/hills.car \
    scripts/load_plain.car

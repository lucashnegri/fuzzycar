#ifndef PID_H
#define PID_H

class PID {
public:
    // Constructor. See the parameters below
    PID(double kp, double ki, double kd, double maxErr);

    // Performs one control step
    double control(double ref, double atual, double step);

private:
    double val;         // current value
    double accErr;     // accumulated error
    double maxErr;     // error limit
    double kp;          // proportional gain
    double ki;          // integral gain
    double kd;          // derivative gain
};

#endif // PID_H

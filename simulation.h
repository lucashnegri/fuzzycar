#ifndef SIMULATION_H
#define SIMULATION_H

#include "car.h"

const double SIM_STEP = 30e-3; // 30ms

class Simulation {
public:
    // takes the ownership of the cars
    Simulation(Car *car_, Car *guide_);
    virtual ~Simulation();

    void simulate();
    void setGuideSpeed(double speed);
    void setGuideLoad(double load);
    void setGuideInitialPosition(double pos);
    void setCarLoad(double load);
    void setTargetDistance(double distance);
    void restart(Car* newCar = 0, Car* newGuide = 0);

    double getElapsedTime() const;
    double getStep() const;
    const Car* getCar() const;
    const Car* getGuide() const;

private:
    Car *car, *guide;
    double step;
    double elapsedTime;
    double targetDistance;
    double guideSpeed, relSpeed;
};

#endif // SIMULATION_H

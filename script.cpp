#include "script.h"

Script::Script(Car* car, Car* guide)
    : sim(car, guide) {

}

Script::~Script() {

}

// returns true when it loaded a valid script, false otherwise
bool Script::load(const QString& path) {
    QFile file(path);
    if( !file.open(QFile::ReadOnly) ) return false;

    QTextStream ts(&file);

    ts >> nLines >> targetDistance;
    if(nLines <= 0 || targetDistance < 0) return false;

    lines = QVector<ScriptLine>(nLines);

    for(int l = 0; l < nLines; ++l) {
        ts >> lines[l].pos;   if(ts.atEnd()) return false;
        ts >> lines[l].speed; if(ts.atEnd()) return false;
        ts >> lines[l].load;
    }

    endPosition = lines.last().pos;

    return true;
}

// returns true when it reached the end of the line. O(log n)
bool Script::query(double pos, double &speed, double &load) const {
    // guard agains spurious negative positions
    if(pos < 0) pos = 0;

    // binary search to the lower bound (insert position)
    QVector<ScriptLine>::ConstIterator it = qLowerBound(lines.begin(), lines.end(), ScriptLine(pos));
    if( it == lines.begin() ) return true; // empty script?

    bool ended = it == lines.end();
    --it;
    speed = it->speed;
    load  = it->load;

    return ended;
}

void Script::simulate() {
    sim.restart();

    int simSteps = 0;
    bool ended = false;

    QVector<double> keys, vals;
    double error = 0, minCur = 1e9, maxCur = -1e9;

    // initialize simulation
    sim.setTargetDistance(targetDistance);
    sim.setGuideInitialPosition(targetDistance);

    while (!ended && simSteps < MAX_STEPS) {
        // guide
        {
            double speed, load;
            query( sim.getGuide()->getPosition(), speed, load);
            sim.setGuideSpeed(speed);
            sim.setGuideLoad(load);
        }

        // controlled car
        {
            double speed, load;
            ended = query( sim.getCar()->getPosition(), speed, load);
            sim.setCarLoad(load);
        }

        // run one simulation step
        sim.simulate();
        double dist = sim.getGuide()->getPosition() - sim.getCar()->getPosition() - targetDistance;
        error += fabs(dist) * sim.getStep();
        ++simSteps;

        // store for latter analysis
        keys.append( sim.getElapsedTime() );
        vals.append( dist );
        double cur = sim.getCar()->getCurrent();
        if(cur < minCur)
            minCur = cur;
        else if(cur > maxCur)
            maxCur = cur;

        // signals the current progress
        emit progress(sim.getCar()->getPosition() / endPosition );
    }

    double totalDist = sim.getCar()->getPosition();
    error /= sim.getElapsedTime();

    // signal that it finished the simulation, passing the results
    emit finished(totalDist, sim.getElapsedTime(), error, keys, vals, minCur, maxCur);
}


ScriptLine::ScriptLine(){
}

ScriptLine::ScriptLine(double pos_) : pos(pos_), speed(1e9), load(1e9) {
}

bool ScriptLine::operator<(const ScriptLine &o) const {
    if(pos   != o.pos)   return pos < o.pos;
    if(speed != o.speed) return speed < o.speed;
    return load < o.load;
}

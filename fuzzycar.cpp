#include "fuzzycar.h"
#include "mathutils.h"

FuzzyCar::FuzzyCar(double R, double L, double K, double b, double J, double _rate)
    : Car(R, L, K, b, J, _rate){

}

FuzzyCar::~FuzzyCar() {

}

void FuzzyCar::simulateGuide(double stepSize, double targetSpeed) {
    targetSpeed /= rate;
    motor.sim(stepSize);
    double err = limit(targetSpeed - motor.getSpeed(), -ERR_LIMIT_RAD, ERR_LIMIT_RAD);
    double cur = limit(motor.getCurrent(), -CUR_MAX, CUR_MAX);
    double dva = speedCtrl.control(err, cur) * stepSize;
    motor.V = limit(motor.V + dva, -VA_MAX, VA_MAX);
}

void FuzzyCar::simulateFollow(double stepSize, double distance, double relSpeed) {
    distance = limit(distance, DIST_MIN, DIST_MAX);
    relSpeed = limit(relSpeed, -SPEED_MAX, SPEED_MAX);

    double dSpeed = distCtrl.control(distance, relSpeed);
    simulateGuide(stepSize, getSpeed() + dSpeed);
}

#include "progressdialog.h"
#include "ui_progressdialog.h"
#include "mathutils.h"

ProgressDialog::ProgressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressDialog) {
    ui->setupUi(this);
}

ProgressDialog::~ProgressDialog() {
    delete ui;
}

void ProgressDialog::progress(double val) {
    ui->progressBar->setValue(limit(val*100,0,100));
}

void ProgressDialog::finish() {
    close();
}

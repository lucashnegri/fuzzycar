#ifndef PIDCAR_H
#define PIDCAR_H

#include "car.h"
#include "pid.h"
#include <cmath>

const double LOW_CUR = -27;
const double HIGH_CUR = 27;

class PIDCar : public Car
{
public:
    PIDCar(double R, double L, double K, double b, double J, double _rate);
    virtual ~PIDCar();

    virtual void simulateGuide(double stepSize, double targetSpeed);
    virtual void simulateFollow(double stepSize, double distance, double relSpeed);

protected:
    PID pidSpeed, pidDistance;
};

#endif // PIDCAR_H

fl::Engine* engine = new fl::Engine;
engine->setName("ControladorDistancia");
engine->addHedge(new fl::Any);
engine->addHedge(new fl::Extremely);
engine->addHedge(new fl::Not);
engine->addHedge(new fl::Seldom);
engine->addHedge(new fl::Somewhat);
engine->addHedge(new fl::Very);

fl::InputVariable* inputVariable1 = new fl::InputVariable;
inputVariable1->setName("ErroDistancia");
inputVariable1->setRange(-5.000, 20.000);

inputVariable1->addTerm(new fl::Trapezoid("MuitoNegativo", -5.000, -5.000, -1.000, 0.000));
inputVariable1->addTerm(new fl::Triangle("Negativo", -1.500, 0.000, 6.000));
inputVariable1->addTerm(new fl::Triangle("Positivo", 0.000, 8.000, 16.000));
inputVariable1->addTerm(new fl::Triangle("MuitoPositivo", 8.000, 20.000, 20.000));
engine->addInputVariable(inputVariable1);

fl::InputVariable* inputVariable2 = new fl::InputVariable;
inputVariable2->setName("Velocidade");
inputVariable2->setRange(-20.000, 20.000);

inputVariable2->addTerm(new fl::Trapezoid("Devagar", -20.000, -20.000, -3.000, 0.000));
inputVariable2->addTerm(new fl::Triangle("Igual", -3.000, 0.000, 3.000));
inputVariable2->addTerm(new fl::Trapezoid("Rapido", 0.000, 3.000, 20.000, 20.000));
engine->addInputVariable(inputVariable2);

fl::OutputVariable* outputVariable1 = new fl::OutputVariable;
outputVariable1->setName("DeltaVel");
outputVariable1->setRange(-20.000, 20.000);
outputVariable1->setLockOutputRange(true);
outputVariable1->setDefaultValue(0.000);
outputVariable1->setLockValidOutput(false);
outputVariable1->setDefuzzifier(new fl::Centroid(200));
outputVariable1->fuzzyOutput()->setAccumulation(new fl::Maximum);

outputVariable1->addTerm(new fl::Triangle("ReduzirMuito", -20.000, -20.000, -18.000));
outputVariable1->addTerm(new fl::Triangle("Reduzir", -11.000, -7.000, -3.000));
outputVariable1->addTerm(new fl::Triangle("Manter", -5.000, 0.000, 5.000));
outputVariable1->addTerm(new fl::Triangle("Aumentar", 3.000, 7.000, 11.000));
outputVariable1->addTerm(new fl::Triangle("AumentarMuito", 18.000, 20.000, 20.000));
engine->addOutputVariable(outputVariable1);

fl::RuleBlock* ruleblock1 = new fl::RuleBlock;
ruleblock1->setName("");
ruleblock1->setConjunction(new fl::Minimum);
ruleblock1->setDisjunction(new fl::Maximum);
ruleblock1->setActivation(new fl::Minimum);

ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is MuitoNegativo then DeltaVel is ReduzirMuito", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is MuitoPositivo then DeltaVel is AumentarMuito", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is Negativo and Velocidade is Devagar then DeltaVel is Aumentar", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is Negativo and Velocidade is Igual then DeltaVel is Manter", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is Negativo and Velocidade is Rapido then DeltaVel is Reduzir", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is Positivo and Velocidade is Devagar then DeltaVel is AumentarMuito", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is Positivo and Velocidade is Igual then DeltaVel is Aumentar", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroDistancia is Positivo and Velocidade is Rapido then DeltaVel is Manter", engine));
engine->addRuleBlock(ruleblock1);


#include "mathutils.h"

double limit(double val, double l, double u) {
    if(val > u) return u;
    if(val < l) return l;
    return val;
}

void rk4( void (*f)(const void*, double*), const void* o, double* y, int n, double h) {
    static double tmp[ODS * 4];
    static double *a = &tmp[0], *b = &tmp[ODS], *c = &tmp[ODS*2], *d = &tmp[ODS*3];

    int i;
    for(i = 0; i < n; ++i) a[i] = y[i];
    f(o, a);

    for(i = 0; i < n; ++i) b[i] = y[i] + a[i] * (h / 2);
    f(o, b);

    for(i = 0; i < n; ++i) c[i] = y[i] + b[i] * (h / 2);
    f(o, c);

    for(i = 0; i < n; ++i) d[i] = y[i] + c[i] * h;
    f(o, d);

    for(i = 0; i < n; ++i) y[i] += (h/6) * (a[i] + 2*b[i] + 2*c[i] + d[i]);
}


# FuzzyCar

Implementação das estratégias de controle fuzzy e PID utilizada no trabalho _Controle Fuzzy de Velocidade e Distância para Carros Elétricos Simplificados_, publicado no III Congresso Brasileiro de Sistemas Fuzzy.

# Requisitos

* Qt 4 ou 5
* FuzzyLite 4

# Compilação

No diretório do projeto (fuzzycar):

    $ cd ..
    $ mkdir fuzzycar-build
    $ cd fuzzycar-build
    $ qmake ../fuzzycar
    $ make -j
    $ ./FuzzyCars
#ifndef SCRIPTDIALOG_H
#define SCRIPTDIALOG_H

#include <QDialog>
#include <QVector>
#include <QFileDialog>
#include <QFile>
#include "qcustomplot.h"

namespace Ui {
class ScriptDialog;
}

class ScriptDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ScriptDialog(QWidget *parent = 0);
    ~ScriptDialog();

    void showResults(const QString& totalDist, const QString &totalTime, const QString& error,
                     const QString& curPeaks, const QVector<double>& keys,
                     const QVector<double>& vals);

private slots:
    void saveResults();

private:
    Ui::ScriptDialog *ui;
    QVector<double> skeys, svals;
};

#endif // SCRIPTDIALOG_H

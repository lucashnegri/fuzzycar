#ifndef FUZZYCAR_H
#define FUZZYCAR_H

#include "car.h"
#include "fuzzyspeedcontroller.h"
#include "fuzzydistancecontroller.h"

const double DIST_MIN = -5, DIST_MAX = 20; // m
const double ERR_LIMIT_RAD  = 200.0;       // rad/s
const double SPEED_MAX      = 15.0;        // m/s

class FuzzyCar : public Car
{
public:
    FuzzyCar(double R, double L, double K, double b, double J, double _rate);
    virtual ~FuzzyCar();
    virtual void simulateGuide(double stepSize, double targetSpeed);
    virtual void simulateFollow(double stepSize, double distance, double relSpeed);

protected:
    FuzzySpeedController speedCtrl;
    FuzzyDistanceController distCtrl;
};

#endif // FUZZYCAR_H

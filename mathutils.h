#ifndef MATHUTILS_H
#define MATHUTILS_H

// Maximum number of state variables of a system
#define ODS 10

// Fourth order Runge-Kutta
void rk4( void (*f)(const void*, double*), const void* o, double* y, int n, double h);

// Limits the value of _val_ to the [l,u] range
double limit(double val, double l, double u);

#endif // MATHUTILS_H

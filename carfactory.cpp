#include "carfactory.h"

Car* CarFactory::newCar(CarType type) {
    double R = 1, L = 0.1, K = 0.5, b = 0.01, J = 0.05, _rate = 0.05;

    switch(type) {
        case FUZZY_CAR:
            return new FuzzyCar(R, L, K, b, J, _rate);

        case PID_CAR:
            return new PIDCar(R, L, K, b, J, _rate);

        default:
            throw "Invalid car type";
            return 0;
    }
}

Car* CarFactory::newGuide(CarType type) {
    double R = 1.2, L = 0.12, K = 0.4, b = 0.01, J = 0.04, _rate = 0.045;

    switch(type) {
        case FUZZY_CAR:
            return new FuzzyCar(R, L, K, b, J, _rate);

        case PID_CAR:
            return new PIDCar(R, L, K, b, J, _rate);

        default:
            throw "Invalid car type";
            return 0;
    }
}

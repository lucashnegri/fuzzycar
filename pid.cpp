#include "pid.h"
#include "mathutils.h"
#include "math.h"

PID::PID(double kp, double ki, double kd, double maxErr) {
    this->val    = 0;
    this->accErr = 0;

    this->kp = kp;
    this->ki = ki;
    this->kd = kd;

    this->maxErr = maxErr;
}

double PID::control(double ref, double cur, double step) {
    double err = limit(ref - cur, -maxErr, maxErr);
    double dif = (cur - val) / step;
    val        = cur;
    accErr    += err * step;

    return err      * kp  /* P */
           + accErr * ki  /* I */
           + dif    * kd; /* D */
}

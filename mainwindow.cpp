#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    diag(this), prog(this), ctrlGroup(this), ui(new Ui::MainWindow) {

    // UI
    ui->setupUi(this);
    ui->actionFuzzy->setActionGroup(&ctrlGroup);
    ui->actionPID->setActionGroup(&ctrlGroup);

    // simulation
    CarType type = getSelectedCarType();
    sim = new Simulation( CarFactory::newCar(type), CarFactory::newGuide(type) );

    connect(&timer, SIGNAL(timeout()), this, SLOT(simulate()) );
    timer.start( sim->getStep() * 1000 );

    // plot configuration
    QCustomPlot* p = ui->plot;
    p->clearGraphs();
    p->addGraph();  // distance
    p->addGraph();  // target distance
    p->xAxis->setLabel("tempo [s]");
    p->yAxis->setLabel(QString::fromUtf8("distância [m]"));
    p->xAxis->setAutoTickStep(false);
    p->xAxis->setTickStep(XTICK_STEP);
    p->yAxis->setRange(0, DISP_DIST);
    p->yAxis->setAutoTickStep(false);
    p->yAxis->setTickStep(YTICK_STEP);
    p->yAxis->setRangeReversed(true);
    p->graph(0)->setPen(QPen(Qt::blue));
    p->graph(1)->setPen(QPen(Qt::red));

    sim->restart();
    updateGUI();
}

MainWindow::~MainWindow() {
    delete sim;
    delete ui;
}

void MainWindow::simulate() {
    if( ui->cbSimulationActive->isChecked() ) {
        double guide_speed     = ui->spinSpeed->value();
        double target_distance = ui->spinDist->value();
        double load            = ui->spinLoad->value();

        sim->setGuideSpeed(guide_speed);
        sim->setTargetDistance(target_distance);
        sim->setCarLoad(load);
        sim->setGuideLoad(load);

        sim->simulate();
        updateGUI();
    }
}

void MainWindow::updateGUI() {
    // update the labels
    const Car* car   = sim->getCar();
    const Car* guide = sim->getGuide();
    double pos       = car->getPosition();
    double gpos      = guide->getPosition();

    ui->lSpeed->setText    ( fmt(car->getSpeed()   ));
    ui->lTension->setText  ( fmt(car->getTension() ));
    ui->lCurrent->setText  ( fmt(car->getCurrent() ));
    ui->lPosition->setText ( fmt(pos / 1000.0     )); // Km

    ui->lGuideSpeed->setText    ( fmt(guide->getSpeed() ));
    ui->lGuidePosition->setText ( fmt(gpos / 1000.0    )); // Km

    double targetDist = ui->spinDist->value();
    ui->lTargetDistance->setText( fmt(targetDist          ));
    ui->lDistance->setText      ( fmt(gpos-pos             ));
    ui->lSimulationTime->setText( fmt(sim->getElapsedTime() ));

    // update the plot
    double t  = sim->getElapsedTime();
    double lt = t - HIST_TIME;
    ui->plot->graph(0)->addData(t, gpos - pos );
    ui->plot->graph(0)->removeDataBefore(lt   );
    ui->plot->graph(1)->addData(t, targetDist);
    ui->plot->graph(1)->removeDataBefore(lt   );

    ui->plot->graph(0)->rescaleKeyAxis(false, false);
    ui->plot->replot();
}

void MainWindow::restartSim() {
    CarType type = getSelectedCarType();
    sim->restart( CarFactory::newCar(type), CarFactory::newGuide(type) );
    sim->setGuideInitialPosition( ui->spinDist->value() );
    ui->plot->graph(0)->clearData();
    ui->plot->graph(1)->clearData();
    updateGUI();
}

void MainWindow::about() {
    QString text = QString::fromUtf8("<html><i>FuzzyCars</i>.<br/><br/>"
                   "<b>Autores:</b> Lucas Hermann Negri e Bruno Ribeiro Sodré</b>.<br/><br/>"
                   "<b>Descrição:</b> realiza o controle de velocidade e de distância<br/>"
                   "entre dois carros elétricos por meio de controladores fuzzy e PID.<br/><br/>"
                   "<i>Março de 2014</i>.</html>");
    QMessageBox::about(this, "Sobre o FuzzyCars", text);
}

void MainWindow::aboutQt() {
    QMessageBox::aboutQt(this);
}

void MainWindow::loadScript() {
    QString path = QFileDialog::getOpenFileName(this, QString::fromUtf8("Roteiro da simulação"),
                                                QString(), "Roteiros (*.car)");
    if( !path.isEmpty() ) {
        CarType type = getSelectedCarType();
        Script* script = new Script( CarFactory::newCar(type), CarFactory::newGuide(type) );

        if( !script->load(path) ) {
            QMessageBox::warning(this, "Erro ao carregar roteiro", QString::fromUtf8("Roteiro inválido"));
            delete script;
            return;
        }

        // pause the current simulation (singletons in fuzzylite...)
        ui->cbSimulationActive->setChecked(false);

        // simulate on another thread
        QThread* thread = new QThread();
        script->moveToThread(thread);
        connect(script, SIGNAL( finished(double,double,double,QVector<double>,QVector<double>,double,double) ),
                this  , SLOT( scriptFinished(double,double,double,QVector<double>,QVector<double>,double,double) ) );

        connect(thread, SIGNAL( started()  ), script, SLOT( simulate() ));
        connect(script, SIGNAL( finished(double,double,double,QVector<double>,QVector<double>,double,double) ),
                thread, SLOT( quit() ));
        connect(script, SIGNAL( finished(double,double,double,QVector<double>,QVector<double>,double,double) ),
                script, SLOT( deleteLater() ));
        connect(thread, SIGNAL( finished() ), thread, SLOT( deleteLater() ));
        connect(script, SIGNAL( progress(double) ), &prog , SLOT( progress(double) ));
        connect(script, SIGNAL( finished(double,double,double,QVector<double>,QVector<double>,double,double) ),
                &prog , SLOT( finish() ));

        thread->start();

        // show progress
        prog.show();
    }
}

void MainWindow::scriptFinished(double totalDist, double totalTime, double error,
                                const QVector<double> &keys, const QVector<double> &vals,
                                double minCur, double maxCur) {
    diag.showResults( fmt(totalDist), fmt(totalTime), fmt(minCur) % QString(" e ") % fmt(maxCur),
                      fmt(error), keys, vals );
}

QString MainWindow::fmt(double val) {
    return locale.toString(val, 'f', 2);
}

CarType MainWindow::getSelectedCarType() {
    if( ui->actionFuzzy->isChecked() ) return FUZZY_CAR;
    if( ui->actionPID->isChecked()   ) return PID_CAR;
    return NONE_CAR;
}

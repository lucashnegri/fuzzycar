#include "fuzzydistancecontroller.h"

FuzzyDistanceController::FuzzyDistanceController() {
    #include "export_distance.cpp"

    fuzzyeng     = engine;
    in_dist      = inputVariable1;
    in_speed     = inputVariable2;
    out_delta_va = outputVariable1;
}

FuzzyDistanceController::~FuzzyDistanceController(){
    delete fuzzyeng;
}

double FuzzyDistanceController::control(double dist, double rel_speed) {
    in_dist->setInputValue(dist);
    in_speed->setInputValue(rel_speed);

    fuzzyeng->process();

    return out_delta_va->defuzzify();
}

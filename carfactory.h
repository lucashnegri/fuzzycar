#ifndef CARFACTORY_H
#define CARFACTORY_H

#include "fuzzycar.h"
#include "pidcar.h"

enum CarType {
    FUZZY_CAR,
    PID_CAR,
    NONE_CAR
};

class CarFactory
{
public:
    // the caller is the owner!
    static Car* newCar(CarType type);
    static Car* newGuide(CarType type);
};

#endif // CARFACTORY_H

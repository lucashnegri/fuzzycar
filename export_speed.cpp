fl::Engine* engine = new fl::Engine;
engine->setName("CarroGuia");
engine->addHedge(new fl::Any);
engine->addHedge(new fl::Extremely);
engine->addHedge(new fl::Not);
engine->addHedge(new fl::Seldom);
engine->addHedge(new fl::Somewhat);
engine->addHedge(new fl::Very);

fl::InputVariable* inputVariable1 = new fl::InputVariable;
inputVariable1->setName("ErroVel");
inputVariable1->setRange(-200.000, 200.000);

inputVariable1->addTerm(new fl::Triangle("Negativo", -200.000, -200.000, 0.000));
inputVariable1->addTerm(new fl::Triangle("Nulo", -90.000, 0.000, 90.000));
inputVariable1->addTerm(new fl::Triangle("Positivo", 0.000, 200.000, 200.000));
engine->addInputVariable(inputVariable1);

fl::InputVariable* inputVariable2 = new fl::InputVariable;
inputVariable2->setName("Corrente");
inputVariable2->setRange(-30.000, 30.000);

inputVariable2->addTerm(new fl::Triangle("Baixa", -30.000, -30.000, -25.000));
inputVariable2->addTerm(new fl::Trapezoid("Normal", -26.500, -20.000, 20.000, 26.500));
inputVariable2->addTerm(new fl::Triangle("Alta", 25.000, 30.000, 30.000));
engine->addInputVariable(inputVariable2);

fl::OutputVariable* outputVariable1 = new fl::OutputVariable;
outputVariable1->setName("DeltaVa");
outputVariable1->setRange(-150.000, 150.000);
outputVariable1->setLockOutputRange(true);
outputVariable1->setDefaultValue(0.000);
outputVariable1->setLockValidOutput(false);
outputVariable1->setDefuzzifier(new fl::Centroid(300));
outputVariable1->fuzzyOutput()->setAccumulation(new fl::AlgebraicSum);

outputVariable1->addTerm(new fl::Trapezoid("ReduzirMuito", -150.000, -150.000, -100.000, -50.000));
outputVariable1->addTerm(new fl::Triangle("Reduzir", -90.000, -50.000, -10.000));
outputVariable1->addTerm(new fl::Triangle("Manter", -50.000, 0.000, 50.000));
outputVariable1->addTerm(new fl::Triangle("Aumentar", 10.000, 50.000, 90.000));
outputVariable1->addTerm(new fl::Trapezoid("AumentarMuito", 50.000, 100.000, 150.000, 150.000));
engine->addOutputVariable(outputVariable1);

fl::RuleBlock* ruleblock1 = new fl::RuleBlock;
ruleblock1->setName("");
ruleblock1->setConjunction(new fl::Minimum);
ruleblock1->setDisjunction(new fl::Maximum);
ruleblock1->setActivation(new fl::Minimum);

ruleblock1->addRule(fl::Rule::parse("if ErroVel is Positivo and Corrente is Normal then DeltaVa is AumentarMuito", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroVel is Negativo and Corrente is Normal then DeltaVa is ReduzirMuito", engine));
ruleblock1->addRule(fl::Rule::parse("if ErroVel is Nulo then DeltaVa is Manter", engine));
ruleblock1->addRule(fl::Rule::parse("if Corrente is Baixa then DeltaVa is Aumentar", engine));
ruleblock1->addRule(fl::Rule::parse("if Corrente is Alta then DeltaVa is Reduzir", engine));
engine->addRuleBlock(ruleblock1);


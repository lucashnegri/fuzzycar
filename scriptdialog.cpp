#include "scriptdialog.h"
#include "ui_scriptdialog.h"

ScriptDialog::ScriptDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScriptDialog) {
    ui->setupUi(this);
    ui->lCurrent->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->lDist->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->lError->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->lTime->setTextInteractionFlags(Qt::TextSelectableByMouse);

    QCustomPlot* p = ui->plot;
    p->xAxis->setLabel("tempo [s]");
    p->yAxis->setLabel("erro [m]");
    p->setInteractions(QCP::iRangeZoom | QCP::iRangeDrag);
}

ScriptDialog::~ScriptDialog() {
    delete ui;
}

void ScriptDialog::showResults(const QString& totalDist, const QString& totalTime, const QString& curPeaks,
                               const QString& error, const QVector<double>& keys, const QVector<double>& vals) {
    // store results
    skeys = keys;
    svals = vals;

    // show info
    ui->lDist->setText( totalDist );
    ui->lTime->setText( totalTime );
    ui->lError->setText( error );
    ui->lCurrent->setText( curPeaks );

    // plot results
    QCustomPlot* p = ui->plot;
    p->clearGraphs();
    p->addGraph();
    p->graph(0)->addData(skeys, svals);
    p->graph(0)->rescaleAxes();
    p->replot();

    show();
}

void ScriptDialog::saveResults() {
    QString path = QFileDialog::getSaveFileName(this, QString::fromUtf8("Salvar resultados numéricos...") );
    if(path.isEmpty()) return;

    QFile file(path);
    if( !file.open(QFile::WriteOnly) ) return;
    QTextStream os(&file);

    for(int i = 0, e = skeys.size(); i < e; ++i)
        os << skeys[i] << " " << svals[i] << "\n";
}

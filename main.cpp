#include <QApplication>
#include <QVector>
#include "mainwindow.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    // register metatypes
    qRegisterMetaType<QVector<double> >("QVector<double>");

    // create and show the main window
    MainWindow w;
    w.show();

    // run application
    return a.exec();
}

#ifndef FUZZYSPEEDCONTROLLER_H
#define FUZZYSPEEDCONTROLLER_H

#include <fl/Headers.h>

class FuzzySpeedController {
public:
    FuzzySpeedController();
    virtual ~FuzzySpeedController();
    virtual double control(double vel_err, double cur);

private:
    fl::Engine *fuzzyeng;
    fl::InputVariable  *in_vel_err, *in_cur;
    fl::OutputVariable *out_delta_va;
};


#endif // FUZZYSPEEDCONTROLLER_H

#include "car.h"
#include <QDebug>

Car::Car(double R, double L, double K, double b, double J, double _rate)
    : motor(R, L, K, b, J), rate(_rate) {
}

Car::~Car() {
}

void Car::setLoad(double load) {
    motor.C = load;
}

double Car::getSpeed() const {
    return motor.getSpeed() * rate;
}

double Car::getTension() const {
    return motor.getTension();
}

double Car::getCurrent() const {
    return motor.getCurrent();
}

double Car::getPosition() const {
    return motor.getPosition() * rate;
}

void Car::setPosition(double pos) {
    motor.setPosition(pos / rate);
}

void Car::restart() {
    motor.restart();
}

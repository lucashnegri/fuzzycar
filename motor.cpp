#include "motor.h"
#include "mathutils.h"

Motor::Motor() {
}

Motor::Motor(double _R, double _L, double _K, double _b, double _J)
    : R(_R), L(_L), K(_K), b(_b), J(_J), V(0), C(0) {
    restart();
}

Motor::~Motor() {
}

double Motor::getCurrent() const {
    return state[0];
}

double Motor::getTension() const {
    return V;
}

double Motor::getSpeed() const {
    return state[1];
}

double Motor::getPosition() const {
    return state[2];
}

void Motor::setPosition(double pos) {
    state[2] = pos;
}

void Motor::sim(double step_size) {
    rk4(motorSystemEq, this, state, 3, step_size);
}

void Motor::restart() {
    state[0] = state[1] = state[2] = 0;
}

void motorSystemEq(const void* pm, double* y) {
    const Motor* m = reinterpret_cast<const Motor*>(pm);

    double Ia = y[0], Wr = y[1];
    double C  = Wr > 0 ? m->C : -m->C;

    y[0] = -m->R/m->L*Ia - m->K/m->L*Wr + (1.0/m->L) * m->V;
    y[1] = m->K/m->J*Ia  - m->b/m->J*Wr - (1.0/m->J) * C;
    y[2] = Wr;
}

#include "fuzzyspeedcontroller.h"

FuzzySpeedController::FuzzySpeedController() {
    #include "export_speed.cpp"

    fuzzyeng     = engine;
    in_vel_err   = inputVariable1;
    in_cur       = inputVariable2;
    out_delta_va = outputVariable1;
}

FuzzySpeedController::~FuzzySpeedController() {
    delete fuzzyeng;
}

double FuzzySpeedController::control(double vel_err, double cur) {
    in_vel_err->setInputValue(vel_err);
    in_cur->setInputValue(cur);
    fuzzyeng->process();
    return out_delta_va->defuzzify();
}

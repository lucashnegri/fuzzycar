#include "pidcar.h"
#include "mathutils.h"

// parâmetros dos PIDS: kp, ki, kd, não modificar os outros.
PIDCar::PIDCar(double R, double L, double K, double b, double J, double _rate)
    : Car(R, L, K, b, J, _rate),
      pidSpeed(1.42, 0.95, 0, 200), pidDistance(1.78, 1.75, 0, 20) {

}

PIDCar::~PIDCar() {

}

void PIDCar::simulateGuide(double stepSize, double targetSpeed) {
    targetSpeed /= rate;
    motor.sim(stepSize);

    double vA = pidSpeed.control(targetSpeed, motor.getSpeed(), stepSize);
    double cur = motor.getCurrent();

    // respect the current limits
    if(cur < LOW_CUR) {
        vA += pow( fabs(cur - LOW_CUR), 2 );
    } else if(cur > HIGH_CUR) {
        vA -= pow( fabs(cur - HIGH_CUR), 2);
    }

    motor.V = limit(vA, -VA_MAX, VA_MAX);
}

void PIDCar::simulateFollow(double stepSize, double distance, double relSpeed) {
    Q_UNUSED(relSpeed);
    double targetSpeed = pidDistance.control(0, -distance, stepSize);
    simulateGuide(stepSize, targetSpeed);
}

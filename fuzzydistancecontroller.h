#ifndef FUZZYDISTANCECONTROLLER_H
#define FUZZYDISTANCECONTROLLER_H

#include <fl/Headers.h>

class FuzzyDistanceController
{
public:
    FuzzyDistanceController();
    virtual ~FuzzyDistanceController();
    virtual double control(double dist, double rel_speed);

private:
    fl::Engine *fuzzyeng;
    fl::InputVariable  *in_dist, *in_speed;
    fl::OutputVariable *out_delta_va;
};

#endif // FUZZYDISTANCECONTROLLER_H

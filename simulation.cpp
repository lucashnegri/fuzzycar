#include "simulation.h"
#include "mathutils.h"

Simulation::Simulation(Car *car_, Car *guide_) : car(car_), guide(guide_), step(SIM_STEP) {
    restart();
}

Simulation::~Simulation() {
    delete car;
    delete guide;
}

void Simulation::restart(Car *newCar, Car *newGuide) {
    if(newCar) {
        delete car;
        car = newCar;
    }

    if(newGuide) {
        delete guide;
        guide = newGuide;
    }

    elapsedTime    = 0;
    targetDistance = 0;
    guideSpeed = relSpeed = 0;
    car->restart();
    guide->restart();
}

void Simulation::simulate() {
    double p1 = car->getPosition(), gp1 = guide->getPosition();

    // simulation step
    guide->simulateGuide(step, guideSpeed);
    car->simulateFollow(step, gp1 - p1 - targetDistance, relSpeed);
    elapsedTime += step;

    // computes the new relative speed
    double p2 = car->getPosition(), gp2 = guide->getPosition();
    relSpeed = -( (gp2 - gp1) - (p2 - p1) ) / step;
}

void Simulation::setGuideSpeed(double speed) {
    guideSpeed = speed;
}

void Simulation::setGuideLoad(double load) {
    guide->setLoad(load);
}

void Simulation::setGuideInitialPosition(double pos) {
    guide->setPosition(pos);
}

void Simulation::setCarLoad(double load) {
    car->setLoad(load);
}

void Simulation::setTargetDistance(double distance) {
    targetDistance = distance;
}

double Simulation::getElapsedTime() const {
    return elapsedTime;
}

double Simulation::getStep() const {
    return step;
}

const Car* Simulation::getCar() const {
    return car;
}

const Car* Simulation::getGuide() const {
    return guide;
}

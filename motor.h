#ifndef MOTOR_H
#define MOTOR_H

class Motor {
public:
    Motor();
    Motor(double _R, double _L, double _K, double _b, double _J);
    virtual ~Motor();

    void sim(double step_size);
    void restart();

    double getSpeed() const;
    double getTension() const;
    double getCurrent() const;
    double getPosition() const;
    void setPosition(double pos);

    double R; // Ohms       , armature resistance
    double L; // H          , armature inductance
    double K; // H
    double b; // N.m.s / rad, viscous friction
    double J; // Kg m²      , moment of inertia
    double V; // V          , armature tension
    double C; // N.m         , load conjugate

private:
    double state[3]; // current, speed, and position
};

// differential equations that models the motor
void motorSystemEq(const void* pm, double* y);

#endif // MOTOR_H

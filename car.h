#ifndef CAR_H
#define CAR_H

#include <QDebug>

#include "motor.h"

const double VA_MAX         = 200.0;       // V
const double CUR_MAX        = 30.0;        // A

// This autonomous electric car has two operation modes:
// 1) Follows a target speed (guide mode)
// 2) Follows another car    (follower mode)
class Car {
public:
    Car(double R, double L, double K, double b, double J, double _rate);
    virtual ~Car();

    virtual void simulateGuide(double stepSize, double targetSpeed) = 0;
    virtual void simulateFollow(double stepSize, double distance, double relSpeed) = 0;

    void setLoad(double load);

    double getSpeed() const;
    double getTension() const;
    double getCurrent() const;
    double getPosition() const;
    void setPosition(double pos);
    void restart();

protected:
    Motor motor;
    double rate;
};

#endif // CAR_H
